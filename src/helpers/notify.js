// outside of a Vue file
import { Notify } from 'quasar'

export function showErrorMessage(message) {
  Notify.create({
    progress: true,
    color: 'negative',
    icon: 'report_problem',
    caption: 'error',
    message: message,
    position: 'top',
    timeout: 10000,
    actions: [{ icon: 'close', color: 'white' }]
  })}

export function showSuccessMessage(message) {
  Notify.create({
    progress: true,
    color: 'secondary',
    icon: 'check',
    caption: 'success',
    message: message,
    position: 'top',
    timeout: 10000,
    actions: [{ icon: 'close', color: 'white' }]
  })}
